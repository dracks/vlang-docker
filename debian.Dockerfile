
from debian:12-slim as base

WORKDIR /opt/vlang

ENV VVV  /opt/vlang
ENV PATH /opt/vlang:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN mkdir -p /opt/vlang && ln -s /opt/vlang/v /usr/bin/v

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive && \
    # Default base depenencies
    apt-get install -y --no-install-recommends git libssl-dev libsqlite3-dev ca-certificates build-essential && \
    # Clean
    apt-get clean && rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*


RUN git clone https://github.com/vlang/v.git /opt/vlang && make && v -version
